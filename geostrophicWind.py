#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 13:24:49 2020

@author: 28805098
"""

import numpy as np
import matplotlib.pyplot as plt
from physProps import *
from differentiate import *

def geostrophicWind(N):
    """calculate the geostrophic wind analytically and numerically and plot"""
    
    #resolution and size of the domain
    #N = 30                      #number of intervals
    ymin = physProps['ymin']
    ymax = physProps['ymax']
    dy = (ymax-ymin)/N          #length of intervals
    
    #the spatial dimension y
    y = np.linspace(ymin, ymax, N+1)
    
    #the pressure at the y points and the exact geostrophic wind
    p = pressure(y, physProps)
    uExact = uGeoExact(y, physProps)
    
    #the pressure gradient and wind using two-point differences
    dpdy = gradient_2point(p, dy)
    u_2point = geoWind(dpdy, physProps)
    
    dpdy3 = gradient_3point(p, dy)
    u_3point = geoWind(dpdy3, physProps)
    
    #graph to compare the analytical and numerical solutions
    font = {'size' : 14}
    plt.rc = ('font', font)
    
    #plot the approximate and exact wind at y points
    plt.plot(y/1000, uExact, 'k-', label = 'Exact')
    plt.plot(y/1000, u_2point, '*r--', label = 'two-point differences', ms = 12,
             markeredgewidth = 1.5, markeredgecolor = 'none')
    plt.legend(loc = 'best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindCent'+str(N)+'.pdf')
    plt.show()

    #plot the errors
    plt.plot(y/1000, u_2point-uExact, '*r--', label = 'Two-point differences', ms = 12,
         markeredgewidth = 1.5, markeredgecolor = 'none')
    plt.legend(loc = 'best')
    plt.axhline(linestyle = '-', color = 'k')
    plt.xlabel('y (km)')
    plt.ylabel('u error (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindErrorsCent'+str(N)+'.pdf')
    plt.show()
    
    
    
    plt.plot(y/1000, uExact, 'k-', label = 'Exact')
    plt.plot(y/1000, u_3point, '.b--', label = 'Three-point differences', ms = 12,
             markeredgewidth = 1.5, markeredgecolor = 'none')
    plt.legend(loc = 'best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindCent_3Point'+str(N)+'.pdf')
    plt.show()

    #plot the errors
    plt.plot(y/1000, u_3point-uExact, '.b--', label = 'Three-point differences', ms = 12,
         markeredgewidth = 1.5, markeredgecolor = 'none')
    plt.legend(loc = 'best')
    plt.axhline(linestyle = '-', color = 'k')
    plt.xlabel('y (km)')
    plt.ylabel('u error (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindErrorsCent_3Point'+str(N)+'.pdf')
    plt.show()
    
    #plot the errors
    plt.plot(y/1000, u_3point-uExact, '.b--', label = 'Three-point differences')
    plt.plot(y/1000, u_2point-uExact, '*r--', label = 'Two-point differences', ms = 12,
         markeredgewidth = 1.5, markeredgecolor = 'none')
    plt.legend(loc = 'best')
    plt.axhline(linestyle = '-', color = 'k')
    plt.xlabel('y (km)')
    plt.ylabel('u error (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindErrorsCent_both'+str(N)+'.pdf')
    plt.show()
    
    #plot the pressure
    plt.plot(y,p)
    plt.show()
    
    
    #initialise total errors for both numerical solutions
    u3TotalError = 0
    u2TotalError = 0
    
    #calculate total error of midpoints for both numerical solutions
    for i in range(1,N-1):
        u3TotalError = u3TotalError +  abs(u_3point[i]-uExact[i])
        u2TotalError = u2TotalError +  abs(u_2point[i]-uExact[i])        
    
    #create a .csv file to store the results
    with open("results.csv", "a") as myfile:
        myfile.write(str(dy) + "," + str(abs(u_2point[0]-uExact[0])) + 
                     ", " + str(abs(u_3point[0]-uExact[0])) + 
          "," + str(abs(u_2point[-1]-uExact[-1])) + "," + 
          str(abs(u_3point[-1]-uExact[-1])) + 
           "," + str(u3TotalError) + "," + str(u2TotalError) + "\n")

if __name__ == "__main__":
    
    with open("results.csv", "a") as myfile:
        myfile.write("deltaY , 2point start error, 3point start error, \
        2point end error, 3point end error, 3point mid error, 2point mid error \n")
      
    for N in range(10,110,10):
        geostrophicWind(N)
    