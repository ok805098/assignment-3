#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 12:48:01 2020

@author: 28805098
"""

# Physical properties and functions for calculating the geostrophic wind
import numpy as np

#A dictionary of physical properties
physProps = {'pa' : 1e5,        # mean pressure
             'pb' : 200. ,      # magnitude of the pressure variations
             'f' : 1e-4,        # coriolis parameter
             'rho' : 1. ,       # density
             'L' : 2.4e6,       # length scale of the pressure variations (k)
             'ymin' : 0. ,      # start of the y domain
             'ymax' : 1e6}      # end of the y domain

def pressure(y, props):
    """The pressure and given y locations based on dictionary of physical
       properties, props"""
    
    pa = props['pa']
    pb = props['pb']
    L = props['L']
    
    return pa + pb*np.cos(y*np.pi/L)

def uGeoExact(y, props):
    """The analytics geostrophic wind speed at given locations, y, based
       on dictionary of physical properties, props"""
       
    pb = props['pb']
    rho = props['rho']
    f = props['f']
    L = props['L']
    
    return pb*np.pi/(rho*f*L) * np.sin(y*np.pi/L)

def geoWind(dpdy, props):
    """The geostrophic wind as a function of pressure gradient based on 
        dictionary of physical properties, props"""
        
    rho = props['rho']
    f = props['f']
    
    return -dpdy/(rho*f)