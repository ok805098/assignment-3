geostrophicWind.py uses differentiate.py and physProps.py to calculate
numerical and exact solutions of the geostrophic wind equation.
geostrophicWind.py takes in a range of values for N (the number of intervals)
and outputs graphs of the numerical solution plotted agains the actual solution
for two numerical methods where the accuracy of the end points differ. The 
code also outputs a file 'results.csv' that gives the relating interval length
along with errors for the end points and total error over the mid-points.