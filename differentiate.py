#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 12:48:59 2020

@author: 28805098
"""

import numpy as np

#functions for calculating gradients

def gradient_2point(f, dx):
    """The gradient of one dimensional array f assuming points are a distance 
        dx apart using 2-point differences. Returns an array the same size as 
        f"""
    
    
    #initialise the array for the gradient to be the same as f
    dfdx = np.zeros_like(f)
    
    #two-point differences at the end points
    dfdx[0] = (f[1]-f[0])/dx     
    dfdx[-1] = (f[-1]-f[-2])/dx
    #centered differences for the mid-points
    for i in range(1, len(f)-1):
        dfdx[i] = (f[i+1]-f[i-1])/(2*dx)
        
    return dfdx

def gradient_3point(f, dx):
    """The gradient of one dimensional array f assuming points are a distance 
        dx apart using un-centred differences. Returns an array the same size as 
        f"""
        #initialise the array for the gradient to be the ame as f
    dfdx3 = np.zeros_like(f)
    
    #un-centred differences at end points
    dfdx3[0] = (4*f[1] - 3*f[0] - f[2])/(2*dx)
    dfdx3[-1] = (3*f[-1] - 4*f[-2] + f[-3])/(2*dx)
    
    
    for i in range(1, len(f)-1):
        dfdx3[i] = (f[i+1]-f[i-1])/(2*dx)
        
    return dfdx3